﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    public InputField inputField;
    public Text text;
    //public GameObject Hit, Up, Down;

    int ans;

    // Start is called before the first frame update
    void Start()
    {
        inputField = inputField.GetComponent<InputField>();
        text = text.GetComponent<Text>();

        ans = Random.Range(0, 100);
        Debug.Log(ans);
    }

    // Update is called once per frame
    void Update()
    {

        //    if (Input.GetKey(KeyCode.A))
        //    {

        //        Hit.SetActive(false);

        //    }

        //    if (Input.GetKey(KeyCode.S))
        //    {

        //        Up.SetActive(false);

        //    }

        //    if (Input.GetKey(KeyCode.D))
        //    {

        //        Down.SetActive(false);

        //    }



        //    if (Input.GetKey(KeyCode.Q))
        //    {

        //        Hit.SetActive(true);

        //    }

        //    if (Input.GetKey(KeyCode.W))
        //    {

        //        Up.SetActive(true);

        //    }

        //    if (Input.GetKey(KeyCode.E))
        //    {

        //        Down.SetActive(true);

        //    }


        if (Input.GetMouseButtonDown(1))
        {
            SceneManager.LoadScene(0);
        }

    }

    public void InputLogger()
    {
        string number = inputField.text;

        int.TryParse(number, out int n);

        if(n == ans)
        {
            text.text = "当たり\n 右クリックでリセット";
        }

        if(n < ans)
        {
            text.text = "小さい";
        }

        if(n > ans)
        {
            text.text = "大きい";
        }


    }


}
